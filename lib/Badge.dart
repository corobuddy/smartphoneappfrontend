import 'package:CoroBuddy/Erfolg.dart';

class Badge extends Erfolge {
  String titel;
  String kurzBeschreibung;
  String bildPfad;

  Badge(this.titel, this.kurzBeschreibung, this.bildPfad);

}