import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'constants.dart';

StatelessWidget loadButtonbar(BuildContext context, List<Widget> buttons, {Color color = lila}) {
  return Container(
      decoration: BoxDecoration(
        color: color,
        border: Border(top: Divider.createBorderSide(context, width: 1.0)),
      ),
      child: ButtonBar(
        children: buttons,
        alignment: MainAxisAlignment.spaceEvenly,
      ));
}

StatelessWidget loadFlatButton(
    BuildContext context, String text, Function onPressed) {
  return new FlatButton(
      onPressed: onPressed,
      child: Container(
        width: 400.w,
        height: 130.h,
        child: Text(text,
            style: Fonts.buttonfont(weiss), textAlign: TextAlign.center),
      ),
      shape: Border.all(color: weiss));
}

StatelessWidget loadCircularButton(
    BuildContext context, String text, Function onPressed) {
  return Container(
      margin: EdgeInsets.all(10.h),
      width: 700.w,
      decoration: BoxDecoration(
          color: rot, borderRadius: BorderRadius.all(Radius.circular(10.0))),
      child: FlatButton(
          onPressed: onPressed,
          child: Container(
              child: Text(
            text,
            style: Fonts.buttonfont(weiss),
          ))));
}

Container loadKasten(Widget text, Color color) {
  return Container(
    width: 850.w,
    decoration: BoxDecoration(
        color: color, borderRadius: BorderRadius.all(Radius.circular(7))),
    child: text,
    padding: EdgeInsets.all(20.w),
    margin: EdgeInsets.fromLTRB(0, 30.h, 0, 30.h),
  );
}
