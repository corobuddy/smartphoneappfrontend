import 'package:CoroBuddy/widgets.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';

import 'BackgroundBuilder.dart';
import 'DataManager.dart';
import 'constants.dart';
import 'ChallengesScreen.dart';
import 'ErfolgeScreen.dart';
import 'SpielregelnScreen.dart';

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    
    setSize(
        MediaQuery.of(context).size.width, MediaQuery.of(context).size.height);
    ScreenUtil.init(context);

    return Scaffold(
        body: GestureDetector(
            onPanUpdate: (details) {
              if (details.delta.dx > 3) {
                changeToChallengesScreen(context);
              } else if (details.delta.dx < -3) {
                changeToSuccessScreen(context);
              }
            },
            child: CustomPaint(
              painter: HomePainter(),
              child: Center(
                  child: Column(children: <Widget>[
                Align(
                    child: Container(
                        margin: EdgeInsets.fromLTRB(0, 20.w, 20.w, 0),
                        child: DropdownButtonHideUnderline(
                            child: DropdownButton(
                                items: [
                              DropdownMenuItem(
                                  value: 0, child: Text("Profil bearbeiten")),
                              DropdownMenuItem(
                                  value: 1, child: Text("Spielregeln")),
                              DropdownMenuItem(value: 2, child: Text("LogOut"))
                            ],
                                onChanged: (value) {
                                  switch (value) {
                                    case 0:
                                      break;
                                    case 1:
                                      changeToSpielregeln(context);
                                      break;
                                    case 2:
                                      break;
                                  }
                                },
                                icon: Icon(Icons.settings),
                                iconSize: 90.w)),
                        alignment: Alignment.topRight)),
                Align(
                  child:
                      Consumer<DataManager>(builder: (context, data, widget) {
                    return Container(
                      width: 750.w,
                      height: 750.w,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        image: DecorationImage(
                            image: AssetImage(DataManager.instance.profilBildPfad),
                            fit: BoxFit.fill),
                      ),
                    );
                  }),
                  alignment: Alignment.center,
                ),
                Consumer<DataManager>(builder: (context, data, widget) {
                  return Text(DataManager.instance.profilName, style: Fonts.normalfont(weiss));
                }),
                Expanded(
                    child: Center(
                        child: Table(
                            columnWidths: Map.from({
                              0: FixedColumnWidth(50.w),
                              1: FixedColumnWidth(750.w),
                              2: FixedColumnWidth(200.w),
                            }),
                            defaultVerticalAlignment:
                                TableCellVerticalAlignment.middle,
                            children: [
                      TableRow(children: [
                        TableCell(child: Container()),
                        TableCell(
                            child: Text("Gemeisterte Challenges",
                                style: Fonts.normalfont(gruen),
                                textAlign: TextAlign.left)),
                        TableCell(
                            child: Text(DataManager.instance.absolvierteChallenges.length.toString(),
                                style: Fonts.normalfont(gruen),
                                textAlign: TextAlign.left))
                      ]),
                      TableRow(children: [
                        Container(height: 100.h),
                        Container(height: 100.h),
                        Container(height: 100.h)
                      ]),
                      TableRow(children: [
                        TableCell(child: Container()),
                        TableCell(
                            child: Text("Tagesfortschritt",
                                style: Fonts.normalfont(gruen))),
                        TableCell(child: Container())
                      ]),
                    ]))),
                Align(
                    alignment: Alignment.bottomCenter,
                    child: loadButtonbar(context, [
                      loadFlatButton(context, "Challenge!",
                          () => changeToChallengesScreen(context)),
                      loadFlatButton(context, "Erfolge",
                          () => changeToSuccessScreen(context))
                    ]))
              ])),
            )));
  }

  void changeToSuccessScreen(BuildContext context) {
    Navigator.push(
        context,
        PageTransition(
            type: PageTransitionType.rightToLeft, child: ErfolgeScreen()));
  }

  void changeToChallengesScreen(BuildContext context) {
    Navigator.push(
        context,
        PageTransition(
            type: PageTransitionType.leftToRight, child: ChallengesScreen()));
  }

  void changeToSpielregeln(BuildContext context) {
    Navigator.push(
        context,
        PageTransition(
            type: PageTransitionType.downToUp, child: SpielregelnScreen()));
  }
}
