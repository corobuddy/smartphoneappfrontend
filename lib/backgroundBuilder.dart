import 'package:flutter/material.dart';

import 'constants.dart';

class HomePainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    Paint paint = Paint();

    Path ovalPath = Path();
    ovalPath.moveTo(-width * 1.2, -height * 0.2);
    ovalPath.quadraticBezierTo(
        width * 0.5, height * 1.4, width * 2.2, -0.2 * height);

    paint.color = rot;
    canvas.drawPath(ovalPath, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return oldDelegate != this;
  }
}

class ProfilPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    Paint paint = Paint();

    Path ovalPath = Path();
    ovalPath.moveTo(-width * 1.7, height * 1.4);
    ovalPath.quadraticBezierTo(
        width * 0.5, -height * 1.0, width * 2.7, 1.4 * height);

    paint.color = gruen;
    canvas.drawPath(ovalPath, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return oldDelegate != this;
  }
}

class GruenObenLinksPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    Paint paint = Paint();

    Path ovalPath = Path();
    ovalPath.moveTo(-width * 1.1, -height * 0.2);
    ovalPath.quadraticBezierTo(
        width * 0.5, height * 0.6, width * 1.5, -0.2 * height);

    paint.color = gruen;
    canvas.drawPath(ovalPath, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return oldDelegate != this;
  }
}

class RotObenLinksPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    Paint paint = Paint();

    Path ovalPath = Path();
    ovalPath.moveTo(-width * 1.1, -height * 0.2);
    ovalPath.quadraticBezierTo(
        width * 0.5, height * 0.6, width * 1.5, -0.2 * height);

    paint.color = rot;
    canvas.drawPath(ovalPath, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return oldDelegate != this;
  }
}

class RotObenPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    Paint paint = Paint();

    Path ovalPath = Path();
    ovalPath.moveTo(-width * 0.8, -height * 0.2);
    ovalPath.quadraticBezierTo(
        width * 0.5, height * 0.6, width * 1.8, -0.2 * height);

    paint.color = rot;
    canvas.drawPath(ovalPath, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return oldDelegate != this;
  }
}

class GruenObenPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    Paint paint = Paint();

    Path ovalPath = Path();
    ovalPath.moveTo(-width * 0.8, -height * 0.2);
    ovalPath.quadraticBezierTo(
        width * 0.5, height * 0.6, width * 1.8, -0.2 * height);

    paint.color = gruen;
    canvas.drawPath(ovalPath, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return oldDelegate != this;
  }
}
