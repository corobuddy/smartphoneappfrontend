import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:page_transition/page_transition.dart';

import 'Challenge.dart';
import 'BackgroundBuilder.dart';
import 'GlueckwunschScreen.dart';
import 'constants.dart';
import 'widgets.dart';
import 'DataManager.dart';

class ChallengeScreen extends StatelessWidget {

  final Challenge challenge;
  final bool active;

  ChallengeScreen(this.challenge, this.active);

  @override
  Widget build(BuildContext context) {
    List<FlatButton> buttons = active? [loadFlatButton(context, "Später", () {Navigator.pop(context);}), loadFlatButton(context, "Abhaken",  () => abhaken(context))] : [loadFlatButton(context, "Zurück", () {Navigator.pop(context);})];
    return Scaffold(
        body: CustomPaint(
            painter: RotObenLinksPainter(),
            child: Column(
                children: <Widget>[
                  Row(
                      children: <Widget>[
                        Container(
                            child:
                                Text(challenge.titel, style: Fonts.headerfont(lila)),
                            margin: EdgeInsets.fromLTRB(50.w, 50.h, 0, 150.h)),
                        Container(
                          height: 350.w,
                          width: 350.w,
                          margin: EdgeInsets.fromLTRB(50.w, 200.h, 0, 0),
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            image: DecorationImage(
                                image: AssetImage(
                                    challenge.bildPfad),
                                fit: BoxFit.fill),
                          ),
                        ),
                      ]),
                  Expanded(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Container(
                            child:
                                Text(challenge.kurzBeschreibung, style: Fonts.normalfont(gruen))),
                        Column(children: <Widget>[
                          Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: <Widget>[Text(challenge.dauer, style: Fonts.untertitelfont(Colors.black))]),
                          Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: <Widget>[Text(challenge.material, style: Fonts.untertitelfont(Colors.black),)])
                        ]),
                        Container(
                          width: 900.w,
                          decoration: BoxDecoration(
                            color: gruen,
                            borderRadius:
                              BorderRadius.all(Radius.circular(10.0))),
                          child: Text(challenge.beschreibung, style: Fonts.kastenfont(weiss)),
                          padding: EdgeInsets.all(20.w),
                            )
                      ],
                    ),
                  ),
                  loadButtonbar(context, buttons),
                ])));
  }

  void abhaken(BuildContext context) {
    Challenge challenge = DataManager.instance.aktiveChallenge;
    DataManager.instance.clearAktiveChallenge();
    Navigator.pushAndRemoveUntil(context, PageTransition(child: GlueckwunschScreen(challenge), type: PageTransitionType.leftToRight), (context) => context.isFirst);
  }
}