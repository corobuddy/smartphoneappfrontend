import 'package:CoroBuddy/DataManager.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'HomeScreen.dart';
import 'ProfilScreen.dart';
import 'DataManager.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await DataManager.instance.load();
  runApp(
    ChangeNotifierProvider(
      create: (context) => DataManager.instance,
      child: MaterialApp(
        routes: {
          '/': (context) => HomeScreen(),
          '/CreateProfile': (context) => ProfilScreen(),
        },
        title: "CoroBuddy",
        initialRoute: ((DataManager.instance.profilName) == '')? '/CreateProfile':'/',
  )));
}