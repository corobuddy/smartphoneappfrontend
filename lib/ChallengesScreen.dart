import 'package:enum_to_string/enum_to_string.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';

import 'BackgroundBuilder.dart';
import 'constants.dart';
import 'widgets.dart';
import 'ChallengeScreenBuilder.dart';
import 'DataManager.dart';
import 'Challenge.dart';
import 'Kategorie.dart';

class ChallengesScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    return Scaffold(
        body: GestureDetector(
            onPanUpdate: (details) {
              if (details.delta.dx < -3) {
                Navigator.pop(context);
              }
            },
            child: CustomPaint(
                painter: GruenObenLinksPainter(),
                child: Consumer<DataManager>(
                builder: (context, data, child) {
                  return Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      ...(DataManager.instance.aktiveChallenge == null)? withoutChallenge(context) : withChallenge(context),
                      loadButtonbar(context, [
                          loadFlatButton(context, "Zurück", () {
                            Navigator.pop(context);
                          })
                        ]),
                    ]);}))));
  }

  withChallenge(BuildContext context) {
    Challenge challenge = DataManager.instance.aktiveChallenge;
    return <Widget>[ 
                      Container(
                          child:
                              Text("Challenge", style: Fonts.headerfont(lila)),
                          margin: headerAbstandLinks),
                      Container(
                        height: 700.w,
                        width: 700.w,
                        margin: EdgeInsets.all(50.h),
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          image: DecorationImage(
                              image:
                                  AssetImage(challenge.bildPfad),
                              fit: BoxFit.fill),
                        ),
                      ),
                      Column(
                        children: <Widget>[
                          Container(
                              child: Text(
                            EnumToString.parseCamelCase(challenge.kategorie),
                            style: Fonts.untertitelfont(rot),
                          )),
                          Container(
                              child: Text(
                            challenge.titel,
                            style: Fonts.normalfont(lila),
                          )),
                        ],
                        mainAxisAlignment: MainAxisAlignment.center,
                      ),
                      loadCircularButton(context, "Annehmen", () {
                            Navigator.push(
                                context,
                                PageTransition(
                                    type: PageTransitionType.downToUp,
                                    child: ChallengeScreen(DataManager.instance.aktiveChallenge, true)));
                        }),];
  }

  withoutChallenge(BuildContext context) {
    return <Widget> [
                      Container(
                        margin: EdgeInsets.fromLTRB(0, 100.h, 100.w, 0),
                          child:
                              Text("keine Challenge verfügbar", style: Fonts.normalfont(lila)),),
                          Container(
                              child: Text(
                            "Leider ist aktuell keine neue Challenge verfügbar. Du musst dich also noch etwas gedulden.",
                            style: Fonts.normalfont(lila), textAlign: TextAlign.center,
                          )),
                      loadCircularButton(context, "Debug", () {
                        //TODO: delete debug option
                          DataManager.instance.aktiveChallenge = Challenge(1, "Rest your ears", "Hör genau hin!", Kategorie.achtsamkeit, "10 Minuten", "Notizbuch, Stift", bsptext, "assets/erfolge/achtsamkeit.png");
                        }),
                        loadCircularButton(context, "Delete Database", () {
                        //TODO: delete debug option
                          DataManager.instance.deleteData();
                        })];
  }
}