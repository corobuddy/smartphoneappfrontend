import 'package:CoroBuddy/Erfolg.dart';
import 'package:CoroBuddy/Kategorie.dart';
import 'package:enum_to_string/enum_to_string.dart';

class Challenge extends Erfolge{
  int id;
  String titel;
  String kurzBeschreibung;
  Kategorie kategorie;
  String dauer;
  String material;
  String beschreibung;
  String bildPfad;

  Challenge(this.id, this.titel, this.kurzBeschreibung, this.kategorie, this.dauer, this.material, this.beschreibung, this.bildPfad);

  toMap() {
    return <String, dynamic>{
      'id': id,
      'titel': titel,
      'kurzBeschreibung': kurzBeschreibung,
      'kategorie': EnumToString.parse(kategorie),
      'dauer': dauer,
      'material': material,
      'beschreibung': beschreibung,
      'bildPfad': bildPfad,
    };
  }

  toStringList() {
    return <String> [id.toString(), titel, kurzBeschreibung, EnumToString.parse(kategorie), dauer, material, beschreibung, bildPfad];
  }

  static toChallengeFromStrings(List<String> strings) {
    return Challenge(int.parse(strings[0]) , strings[1], strings[2], EnumToString.fromString(Kategorie.values, strings[3]), strings[4], strings[5], strings[6], strings[7]);
  }

  static toChallengeFromMap(Map map) {
    return Challenge(map['id'], map['titel'], map['kurzBeschreibung'], EnumToString.fromString(Kategorie.values, 'kategorie'), map['dauer'], map['material'], map['beschreibung'], map['bildPfad']);
  }

  static toChallenges(List<Map> list) {
    List<Challenge> res = List();
    for(Map i in list) res.add(Challenge.toChallengeFromMap(i));
    return res;
  }
}