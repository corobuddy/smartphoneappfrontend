import 'package:CoroBuddy/widgets.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:page_transition/page_transition.dart';

import 'BackgroundBuilder.dart';
import 'DataManager.dart';
import 'SpielregelnScreen.dart';
import 'constants.dart';

class ProfilScreen extends StatefulWidget {

  @override
  State<ProfilScreen> createState() => ProfilScreenState();
}

  class ProfilScreenState extends State<ProfilScreen> {
  
  final controller = TextEditingController();

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    setSize(
        MediaQuery.of(context).size.width, MediaQuery.of(context).size.height);
    ScreenUtil.init(context);
    return Scaffold(
        body: CustomPaint(
            painter: ProfilPainter(),
            child: Center(
              child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,    
              children: <Widget>[
              Expanded(
                flex: 2,
                child: Container(
                child:Text("Profil erstellen", style: Fonts.headerfont(lila)),
                margin: headerAbstandMitte,)),
              Expanded(
                flex: 2,
                child: Container(
                  width: 800.w,
                    child: TextFormField(
                      controller: controller,
                      style: Fonts.normalfont(weiss),
                      decoration: InputDecoration(labelText: 'Name:', labelStyle: Fonts.normalfont(weiss)),
                  ))),
              Expanded(
                flex: 2,
                child:Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget> [
                loadCircularButton(context, "Spielregeln", () {
                  Navigator.push(
                    context,
                    PageTransition(
                        type: PageTransitionType.downToUp,
                        child: SpielregelnScreen()));
                }),
                loadCircularButton(context, "Los Gehts", () {
                  SystemChannels.textInput.invokeMethod('TextInput.hide');
                  DataManager.instance.profilName = controller.text;
                  Navigator.pushReplacementNamed(context, '/');
                })]))
            ]))));
  }
  }