import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'BackgroundBuilder.dart';
import 'constants.dart';
import 'widgets.dart';

class SpielregelnScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: CustomPaint(
      painter: GruenObenPainter(),
      child: Center(
        child: Column(
          children: <Widget>[
            Container(
                margin: headerAbstandMitte,
                child: Text("Spielregeln", style: Fonts.headerfont(weiss))),
            loadKasten(Text(regeln1, style: Fonts.kastenfontklein(weiss)), lila),
            loadKasten(Text(regeln2, style: Fonts.kastenfontklein(weiss)), lila),
            loadKasten(Text(regeln3, style: Fonts.kastenfontklein(weiss)), lila),
            Container(
              width: 850.w,
              decoration: BoxDecoration(
                  color: rot,
                  borderRadius: BorderRadius.all(Radius.circular(10))),
              child: FlatButton(
                  child: Text("Verstanden!", style: Fonts.buttonfont(weiss)),
                  onPressed: () {
                    Navigator.pop(context);
                  }),
              padding: EdgeInsets.all(20),
              margin: EdgeInsets.fromLTRB(0, 30.h, 0, 0),
            )
          ],
        ),
      ),
    ));
  }
}