import 'package:CoroBuddy/Challenge.dart';
import 'package:CoroBuddy/BackgroundBuilder.dart';
import 'package:CoroBuddy/constants.dart';
import 'package:CoroBuddy/widgets.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class GlueckwunschScreen extends StatelessWidget {
  
  final Challenge challenge;

  GlueckwunschScreen(this.challenge);
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: CustomPaint(
            painter: RotObenPainter(),
            child: Center(
                child: Column(
              children: <Widget>[
                Container(
                    child: Text("Challenge Abgehakt",
                        style: Fonts.headerfont(weiss)),
                    margin: headerAbstandMitte),
                    Expanded(child: Container()),
                loadKasten(
                  Center(child: 
                  Column(children: [
                    Text("Glückwunsch!", style: Fonts.kastenfont(weiss)),
                    Text("Du hast die Challenge " + challenge.titel + " erfolgreich absolviert.", style: Fonts.kastenfontklein(weiss)),
                ])), lila),
                Expanded(child: Container()),
                Align( alignment: Alignment.bottomCenter,
                  child: loadButtonbar(context, [
                  loadFlatButton(context, "Zurück", () {
                    Navigator.pop(context);
                    }),
                  loadFlatButton(context, "Teilen", () {})
                ]))
              ],
            ))));
  }
}
