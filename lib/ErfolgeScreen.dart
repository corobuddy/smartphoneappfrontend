import 'package:CoroBuddy/Erfolg.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:page_transition/page_transition.dart';
import 'package:provider/provider.dart';

import 'Badge.dart';
import 'ChallengeScreenBuilder.dart';
import 'DataManager.dart';
import 'BackgroundBuilder.dart';
import 'constants.dart';

class ErfolgeScreen extends StatefulWidget {
  ErfolgeScreen({Key key}) : super(key: key);

  @override
  ErfolgeScreenState createState() => ErfolgeScreenState();
}

class ErfolgeScreenState extends State<StatefulWidget> {
  int _selectedIndex = 0;

  void _onItemTap(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GestureDetector(
          onPanUpdate: (details) {
            if (details.delta.dx > 3) {
              Navigator.pop(context);
            }
          },
          child: CustomPaint(
              painter: RotObenPainter(),
              child: Center(
                  child: CustomScrollView(
                slivers: <Widget>[
                  SliverAppBar(
                    pinned: true,
                    backgroundColor: Colors.transparent,
                    expandedHeight: 600.h,
                    flexibleSpace: FlexibleSpaceBar(
                      title: Text(
                        "Erfolge",
                        style: Fonts.normalfont(lila),
                      ),
                    ),
                  ),
                  Consumer<DataManager>(builder: (context, data, child) {
                    List<List<Erfolge>> listOptions = [
                      DataManager.instance.absolvierteChallenges,
                      [
                        Badge("Corobuddy!", "Angemeldet!",
                            "assets/erfolge/buddy1.png"),
                        Badge(
                            "Ins Rollen gekommen!",
                            "10 Challenges absolviert!",
                            "assets/erfolge/buddy2.png"),
                        Badge("Rock and Roll!", "20 Challenges absolviert!",
                            "assets/erfolge/buddy3.png")
                      ]
                    ];
                    return SliverFixedExtentList(
                        itemExtent: 400.h,
                        delegate: SliverChildBuilderDelegate(
                            (BuildContext context, int index) {
                          return GestureDetector(
                            onTapDown: (details) {
                              Navigator.push(context, PageTransition(child: ChallengeScreen(listOptions[_selectedIndex][index], false), type: PageTransitionType.downToUp));
                            },
                            child: Container(
                              height: 350.h,
                              width: 1080.w,
                              child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: <Widget>[
                                Container(
                                  margin: EdgeInsets.fromLTRB(0, 25.h, 0, 25.h),
                                  width: 350.h,
                                  height: 350.h,
                                  decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    image: DecorationImage(
                                        image: AssetImage(
                                            listOptions[_selectedIndex][index]
                                                .bildPfad),
                                        fit: BoxFit.fill),
                                  ),
                                ),
                                Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Container(
                                        child: Text(
                                            listOptions[_selectedIndex][index]
                                                .kurzBeschreibung,
                                            style: Fonts.untertitelfont(rot),
                                            textAlign: TextAlign.center),
                                      ),
                                      Container(
                                          padding: EdgeInsets.fromLTRB(
                                              10.w, 5.h, 10.w, 5.h),
                                          width: 600.w,
                                          decoration: BoxDecoration(
                                              color: lila,
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(10.0))),
                                          child: Text(
                                              listOptions[_selectedIndex][index]
                                                  .titel,
                                              style: Fonts.kastenfont(weiss),
                                              textAlign: TextAlign.center))
                                    ])
                              ])));
                        }, childCount: listOptions[_selectedIndex].length));
                  }),
                ],
              )))),
      bottomNavigationBar: BottomNavigationBar(
          backgroundColor: lila,
          currentIndex: _selectedIndex,
          onTap: _onItemTap,
          items: <BottomNavigationBarItem>[
            BottomNavigationBarItem(
                icon: Icon(Icons.check),
                title: Text("Abgehakt", style: Fonts.untertitelfont(weiss))),
            BottomNavigationBarItem(
                icon: Icon(Icons.filter_vintage),
                title: Text("Badges", style: Fonts.untertitelfont(weiss)))
          ]),
    );
  }
}
