import 'dart:collection';
import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:CoroBuddy/Challenge.dart';

class DataManager extends ChangeNotifier{

  static final _databaseName = "CoroBuddyChallenges.db";
  static final _databaseVersion = 1;

  DataManager._privateConstructor() {
  }

  load() async {
    await loadProfilName();
    await queryAll();
    await loadAktiveChallenge();
  }

  static final DataManager instance = DataManager._privateConstructor();

  static Database _database;
  Future<Database> get database async {
    if(_database != null) return _database;
    _database = await _initDatabase();
    return _database;
  }

  String _profilName = '';
  String get profilName => _profilName;
  set profilName(String name) {
    _profilName = name;
    saveProfilName();
    notifyListeners();
  }

  saveProfilName() async {
    if(_profilName != '') {
      (await SharedPreferences.getInstance()).setString('profilName', _profilName);
    } else {
      (await SharedPreferences.getInstance()).setString('profilName', null);
    }
  }

  Future<String> loadProfilName() async {
    String tmp = (await SharedPreferences.getInstance()).getString('profilName');
    if(tmp != null && _profilName == '') {
      _profilName = tmp;
    }
    return _profilName;
  }

  String profilBildPfad = "blankprofile.jpg";

  List<Challenge> _absolvierteChallenges;
  UnmodifiableListView<Challenge> get absolvierteChallenges => UnmodifiableListView(_absolvierteChallenges);

  Challenge _aktiveChallenge;
  Challenge get aktiveChallenge => _aktiveChallenge;
  
  set aktiveChallenge(Challenge challenge) {
    _aktiveChallenge = challenge;
    saveAktiveChallenge();
    notifyListeners();
  }

  saveAktiveChallenge() async {
    if(_aktiveChallenge != null) {
      (await SharedPreferences.getInstance()).setStringList('aktiveChallenge', _aktiveChallenge.toStringList());
    } else {
      (await SharedPreferences.getInstance()).setStringList('aktiveChallenge', null);
    }
  }

  Future<Challenge> loadAktiveChallenge() async {
    List<String> tmp = (await SharedPreferences.getInstance()).getStringList('aktiveChallenge');
    if(tmp != null && _aktiveChallenge == null) {
      _aktiveChallenge = Challenge.toChallengeFromStrings(tmp);
    }
    return _aktiveChallenge;
  }

  clearAktiveChallenge() {
    insert(_aktiveChallenge);
    queryAll();
    _aktiveChallenge = null;
    saveAktiveChallenge();
    notifyListeners();
  }

  _initDatabase() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, _databaseName);
    return await openDatabase(path, version: _databaseVersion, onCreate: _onCreate);
  }

  Future _onCreate(Database db, int version) async {
    await db.execute('''CREATE TABLE challenges (
                          id INTEGER PRIMARY KEY,
                          titel TEXT NOT NULL,
                          kurzBeschreibung TEXT NOT NULL,
                          kategorie TEXT NOT NULL,
                          dauer TEXT NOT NULL,
                          material TEXT NOT NULL,
                          beschreibung TEXT NOT NULL,
                          bildPfad TEXT NOT NULL
                      )''');
  }

  Future<int> insert(Challenge challenge) async {
    try {
      return await (await database).insert('challenges', challenge.toMap());
    } catch(e) {
      return 5;
      }
  }

  Future<Challenge> queryChallenge(int id) async {
    return Challenge.toChallengeFromMap((await (await database).rawQuery('SELECT * FROM challenges WHERE id = $id')).elementAt(0));
  }

  queryAll() async {
      _absolvierteChallenges =  Challenge.toChallenges(await (await database).rawQuery('SELECT * FROM challenges'));
  }

  deleteData() async {
    (await database).delete("challenges");
    queryAll();
    notifyListeners();
  }
}