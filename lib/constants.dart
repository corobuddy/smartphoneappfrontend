import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

//Beispieldaten
final int challenges = 21;
final String bsptext ="Mach das Fenster auf und lausche den Geräuschen:\nJetzt wo weniger Leute unterwegs sind:\nWas kannst du alles hören? Wie fühlt sich das an?";

//Sting Objekte
final String regeln1 ="Jeden Tag bekommst du Challenges gestellt:\nChallenge Nummer 1 um 9:00 Uhr\nChallenge Nummer 2 um 13:00 Uhr\nCommunitychallenge um 18:00 Uhr";
final String regeln2 ="Du hast Zeit die Challenge abzuhaken, bis die nächste Herausforderung auf dich zukommt.";
final String regeln3 ="Als Belohnung erhälst du #corobuddy badges, die du sammeln und teilen kannst, und so deine Freunde im Wettstreit gegen die Langeweile über-trumpfen.";

//Farben:
const Color lila = Color.fromARGB(230, 63, 45, 115);
const Color rot = Color.fromARGB(230, 244, 87, 86);
const Color gruen = Color.fromARGB(230, 74, 192, 187);
const Color weiss = Color.fromARGB(230, 255, 255, 255);

//Abstände:
final EdgeInsets headerAbstandLinks = EdgeInsets.fromLTRB(100.w, 100.h, 300.w, 100.h);
final EdgeInsets headerAbstandMitte = EdgeInsets.all(80.w);

//Fonts:
class Fonts{
  
  static TextStyle buttonfont(Color color) {
    return GoogleFonts.benchNine(color: color, fontSize: 100.sp);
  }
  static TextStyle headerfont(Color color){
    return GoogleFonts.benchNine(color: color, fontWeight: FontWeight.bold, fontSize: 140.sp);
  }
  static TextStyle untertitelfont(Color color){
    return GoogleFonts.benchNine(color: color, fontSize: 50.sp);
  }
  static TextStyle normalfont(Color color){
    return GoogleFonts.benchNine(color: color, fontWeight: FontWeight.bold, fontSize: 100.sp);
  }
  static TextStyle kastenfont(Color color){
    return GoogleFonts.benchNine(color: color, fontSize: 70.sp);
  }
  static TextStyle kastenfontklein(Color color){
    return GoogleFonts.benchNine(color: color, fontSize: 60.sp);
  }
}

//size
double width;
double height;

void setSize(double width1, double height1) {
  width = width1;
  height = height1;
}